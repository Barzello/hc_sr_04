/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "tim.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdbool.h>
#include "ssd1306.h"
#include "fonts.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define TRIGER_DISTANCE_VALUE_ENTER			(15) //distance value for enter sensor in santimeters
#define TRIGER_DISTANCE_VALUE_EXIT			(15) //distance value for exit sensor in santimeters
#define MAX_PEOPLE_EXSIST_TIMEOUT			(2000) //max timeout while people can be between sensors in milliseconds

#define TURN_ON 							(1)
#define TURN_OFF							(0)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
uint32_t IC_Val1 = 0;
uint32_t IC_Val2 = 0;
uint8_t Is_First_Captured = 0;  // is the first value captured ?
uint8_t Distance_enter  = 0; // value from sensor on enter
uint32_t Difference = 0;

uint32_t IC_Val1_2 = 0;
uint32_t IC_Val2_2 = 0;
uint8_t Is_First_Captured_2 = 0;  // is the first value captured ?
uint8_t Distance_exit  = 0; //value from sensor on exit
uint32_t Difference_2 = 0;

uint16_t people_cnt=0; //counter for people
bool enable_plp_for_enter=false; //bool for triggered ssensor on enter
bool enable_plp_for_exit=false; //bool for triggered people on exit

uint32_t timer_enter=0; //time check after sensor on enter is triggered
uint32_t timer_exit=0; // time check after sensor on exit is triggered

uint16_t Max_people_cnt=10; //default value
uint8_t Central_btn_status=0;

I2C_HandleTypeDef hi2c1; //screen connection data //init of i2c protocol to work with screen

char buffer1[10] = {'\0'}; //buffer with data for screen
char buffer2[10] = {'\0'}; //buffer with data for screen
char buffer3[10] = {'\0'}; //buffer with data for screen
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void); //google it
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
void delay (uint16_t time) //custom delay to work with microseconds //Hal_Delay works only with milliseconds
{
	__HAL_TIM_SET_COUNTER(&htim1, 0);
	while (__HAL_TIM_GET_COUNTER (&htim1) < time);
}

void clear_buff(char* buff,uint8_t size) //function for clear screen buffer data
{
	for(uint8_t i=0;i<size;i++)
	{
		buff[i]='\0';
	}
}

void update_screen(void)
{
	ssd1306_Fill(Black);
	clear_buff(buffer1,10);
	clear_buff(buffer2,10);
	ssd1306_SetCursor(0, 0);
	ssd1306_WriteString("Hello", Font_7x10, White);

	ssd1306_SetCursor(0, 18);
	ssd1306_WriteString("In place:", Font_7x10, White);

	ssd1306_SetCursor(0, 36);
	ssd1306_WriteString("MAX in place:", Font_7x10, White);


	ssd1306_SetCursor(90, 18);
	sprintf(buffer1,"%d", people_cnt);
	ssd1306_WriteString(buffer1, Font_7x10, White);

	ssd1306_SetCursor(90, 36);
	sprintf(buffer1,"%d", Max_people_cnt);
	ssd1306_WriteString(buffer1, Font_7x10, White);

	ssd1306_SetCursor(50, 0);
	sprintf(buffer3,"%d / %d", Distance_enter, Distance_exit);
	ssd1306_WriteString(buffer3, Font_7x10, White);

	ssd1306_UpdateScreen(&hi2c1);
}

int test_func(int a)
{
	a = 10;
	return a;
}

void HCSR04_Read_Enter (void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_SET);  // pull the TRIG pin HIGH
	delay(10);  // wait for 10 us
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_9, GPIO_PIN_RESET);  // pull the TRIG pin low

	__HAL_TIM_ENABLE_IT(&htim1, TIM_IT_CC1);
}

void HCSR04_Read_Exit (void)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_SET);  // pull the TRIG pin HIGH
	delay(10);  // wait for 10 us
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_11, GPIO_PIN_RESET);  // pull the TRIG pin low

	__HAL_TIM_ENABLE_IT(&htim1, TIM_IT_CC3);
}

void TURN_GREEN_LED(GPIO_PinState Val)
{
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_8, Val);
}

void TURN_RED_LED(GPIO_PinState Val)
{
	HAL_GPIO_WritePin(GPIOC, GPIO_PIN_9, Val);
}

void TURN_SOUND(GPIO_PinState Val)
{
	HAL_GPIO_WritePin(GPIOA, GPIO_PIN_1, Val);
}

bool Get_BTN_Red_Data(void)
{
	bool value = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_8);
	return !value;
}

bool Get_BTN_Yellow_Data(void)
{
	bool value = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_10);
	return !value;
}

bool Get_BTN_Green_Data(void)
{
	bool value = HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_9);
	return !value;
}

bool Button_filter(bool (*fun_ptr)(void)) //false push btn detector
{
	uint8_t max_tries = 10;
	uint8_t yes = 0,no = 0;
	bool result=0;
	for (uint8_t i = 0;i<max_tries;i++)
	{
		if (fun_ptr())
			yes += 1;
		else
			no += 1;
		HAL_Delay(5);
	}
	if (yes>0.8*max_tries)
		result = 1;
	else
		result = 0;
	return result;
}

void Set_Max_People_Value(void)
{
	if(Button_filter(Get_BTN_Yellow_Data) && Central_btn_status==0)
	{
		Central_btn_status=1;
	}
	else if(Button_filter(Get_BTN_Yellow_Data) && Central_btn_status==1)
	{
		Central_btn_status=0;
	}

	if(Central_btn_status==1)
	{
		if(Button_filter(Get_BTN_Red_Data))
		{
			Max_people_cnt+=1;
		}
		if(Button_filter(Get_BTN_Green_Data))
		{
			if (Max_people_cnt<=0) Max_people_cnt=0;
			else Max_people_cnt-=1;
		}
	}
	else if (Central_btn_status==0)
	{

		if(Button_filter(Get_BTN_Red_Data))
		{
			people_cnt+=1;
		}
		if(Button_filter(Get_BTN_Green_Data))
		{
			if (people_cnt<=0) Max_people_cnt=0;
			else people_cnt-=1;
		}
	}

}

void People_CNT_Compare(void)
{
	if ((Distance_enter<TRIGER_DISTANCE_VALUE_ENTER) && (enable_plp_for_exit==false)
													 && (enable_plp_for_enter==false))
	{
		timer_enter=HAL_GetTick();
		enable_plp_for_enter=true;
	}
	else if ((Distance_exit<TRIGER_DISTANCE_VALUE_EXIT) && (enable_plp_for_enter==false)
														&& (enable_plp_for_exit==false))
	{
		timer_exit=HAL_GetTick();
		enable_plp_for_exit=true;
	}

	if (enable_plp_for_enter==true) //if sensor on enter triggered first
	{
		if (Distance_exit<TRIGER_DISTANCE_VALUE_EXIT)
		{
			people_cnt+=1;
			enable_plp_for_enter=false;
			enable_plp_for_exit=false;
		}
		else if ((HAL_GetTick()-timer_enter)>MAX_PEOPLE_EXSIST_TIMEOUT)
		{
			enable_plp_for_enter=false;
			enable_plp_for_exit=false;
		}

	}

	else if (enable_plp_for_exit==true) //if sensor on exit triggered first
	{
		if (Distance_enter<TRIGER_DISTANCE_VALUE_EXIT)
		{
			people_cnt-=1;
			enable_plp_for_exit=false;
			enable_plp_for_enter=false;
		}
		else if ((HAL_GetTick()-timer_exit)>MAX_PEOPLE_EXSIST_TIMEOUT)
		{
			enable_plp_for_exit=false;
			enable_plp_for_enter=false;
		}
	}

}

void People_CNT_Scheduler(void)
{
	if (people_cnt>Max_people_cnt)
	{
		TURN_RED_LED(TURN_ON);
		TURN_GREEN_LED(TURN_OFF);
		TURN_SOUND(TURN_ON);
	}
	else
	{
		TURN_RED_LED(TURN_OFF);
		TURN_GREEN_LED(TURN_ON);
		TURN_SOUND(TURN_OFF);
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_TIM1_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */


	if (ssd1306_Init(&hi2c1) != 0) //if screen not connected
	{
	  Error_Handler();
	}

	ssd1306_Fill(Black); //fill screen with start color
	HAL_Delay(1000);
	ssd1306_UpdateScreen(&hi2c1);

	TURN_GREEN_LED(TURN_ON);

	HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_1); //start getting data from 1st sensor (enter)
	HAL_TIM_IC_Start_IT(&htim1, TIM_CHANNEL_3); //start getting data from 2d sensor (exit)
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  //read distance
	HCSR04_Read_Enter(); //get distance from enter sensor
	HCSR04_Read_Exit(); //get distance from exit sensor
	Set_Max_People_Value(); //set max or casual people cnt
	People_CNT_Compare(); //wait for triggered second sensor
	People_CNT_Scheduler(); //compare people cnt with max people cnt
	update_screen(); // view people cnt on display
	HAL_Delay(200);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */

  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim)
{
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_1)  // if the interrupt source is channel1
	{
		if (Is_First_Captured==0) // if the first value is not captured
		{
			IC_Val1 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1); // read the first value
			Is_First_Captured = 1;  // set the first captured as true
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_FALLING);
		}
		else if (Is_First_Captured==1)   // if the first is already captured
		{
			IC_Val2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_1);  // read second value
			__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter
			if (IC_Val2 > IC_Val1)
			{
				Difference = IC_Val2-IC_Val1;
			}
			else if (IC_Val1 > IC_Val2)
			{
				Difference = (0xffff - IC_Val1) + IC_Val2;
			}
			Distance_enter = Difference * .034/2;
			Is_First_Captured = 0; // set it back to false
			// set polarity to rising edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_1, TIM_INPUTCHANNELPOLARITY_RISING);
			__HAL_TIM_DISABLE_IT(&htim1, TIM_IT_CC1);
		}
		else
		{

		}
	}
	if (htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3)  // if the interrupt source is channel3
	{
		if (Is_First_Captured_2==0) // if the first value is not captured
		{
			IC_Val1_2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3); // read the first value
			Is_First_Captured_2 = 1;  // set the first captured as true
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_3, TIM_INPUTCHANNELPOLARITY_FALLING);
		}
		else if (Is_First_Captured_2==1)   // if the first is already captured
		{
			IC_Val2_2 = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);  // read second value
			__HAL_TIM_SET_COUNTER(htim, 0);  // reset the counter
			if (IC_Val2_2 > IC_Val1_2)
			{
				Difference_2 = IC_Val2_2-IC_Val1_2;
			}
			else if (IC_Val1_2 > IC_Val2_2)
			{
				Difference_2 = (0xffff - IC_Val1_2) + IC_Val2_2;
			}
			Distance_exit = Difference_2 * .034/2;
			Is_First_Captured_2 = 0; // set it back to false
			// set polarity to rising edge
			__HAL_TIM_SET_CAPTUREPOLARITY(htim, TIM_CHANNEL_3, TIM_INPUTCHANNELPOLARITY_RISING);
			__HAL_TIM_DISABLE_IT(&htim1, TIM_IT_CC3);
		}
		else
		{

		}
	}
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
